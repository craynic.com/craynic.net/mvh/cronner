FROM alpine:3.21@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

COPY files/ /

# renovate: datasource=repology depName=alpine_3_21/supercronic depType=dependencies versioning=loose
ARG SUPERCRONIC_VERSION="0.2.33-r2"

RUN apk add --no-cache \
        supercronic="${SUPERCRONIC_VERSION}" \
        bash~=5 \
        run-parts~=4 \
        moreutils~=0 \
        findutils~=4 \
        coreutils~=9 \
        supervisor~=4 \
        logrotate~=3 \
        curl~=8

ENV APP_CRONTAB_RELATIVE_PATH="/etc/crontab" \
    APP_LOG_FILE_RELATIVE_PATH="/logs/cron/cron.log" \
    APP_HEARTBEAT_ENDPOINT="" \
    APP_HEARTBEAT_CRON="* * * * *"

ENTRYPOINT ["/usr/local/sbin/docker-entrypoint.sh"]
CMD ["/usr/local/sbin/supervisord.sh"]
