#!/usr/bin/env bash

set -Eeuo pipefail

source "${BASH_SOURCE%/*}/common.sh"

find "$PROJECTS_ROOT" -wholename "*$CRONTAB_RELATIVE_PATH" -type f | sort | while read -r SOURCE_CRONTAB_FILENAME; do
  SITE_NAME="$(basename -- "${SOURCE_CRONTAB_FILENAME%"$CRONTAB_RELATIVE_PATH"}")"
  SUPERVISOR_CONFIG_FILENAME="$(getSuperVisorConfigFileName "$SITE_NAME")"
  TARGET_CRONTAB_FILENAME="$(getTargetCrontabFileName "$SITE_NAME")"

  # crontab did not change
  if cmp -s "$SOURCE_CRONTAB_FILENAME" "$TARGET_CRONTAB_FILENAME"; then
    continue
  fi

  # create target crontab if needed
  if [[ ! -f "$TARGET_CRONTAB_FILENAME" ]]; then
    SITE_ID="$(getSiteID "$SITE_NAME")"

    touch -- "$TARGET_CRONTAB_FILENAME"
    chmod 0640 -- "$TARGET_CRONTAB_FILENAME"
    chgrp "$SITE_ID" -- "$TARGET_CRONTAB_FILENAME"
  fi

  # update target crontab
  cat "$SOURCE_CRONTAB_FILENAME" > "$TARGET_CRONTAB_FILENAME"

  # empty crontab
  if ! assertTargetCrontabNotEmpty "$SITE_NAME"; then
    echo "New crontab \"$SOURCE_CRONTAB_FILENAME\" is empty, not making any changes" | logForSite "$SITE_NAME"
    stopCronForSite "$SITE_NAME"
  # invalid crontab
  elif ! validateTargetCrontab "$SITE_NAME"; then
    echo "New crontab \"$SOURCE_CRONTAB_FILENAME\" invalid, not making any changes" | logForSite "$SITE_NAME"
  # the config does not exist => crontab is new
  elif [[ ! -f "$SUPERVISOR_CONFIG_FILENAME" ]]; then
    echo "New crontab \"$SOURCE_CRONTAB_FILENAME\" created, starting cron" | logForSite "$SITE_NAME"
    createSuperVisorConfig "$SITE_NAME"
  else
    echo "Crontab \"$SOURCE_CRONTAB_FILENAME\" changed, reloading cron" | logForSite "$SITE_NAME"
    reloadCronForSite "$SITE_NAME"
  fi
done

# identify deleted crontabs
find "$PROJECTS_ROOT" -mindepth 1 -maxdepth 1 -type d "!" -execdir test -f "{}${CRONTAB_RELATIVE_PATH}" ";" -print \
  | while read -r SITE_ROOT; do
    SITE_NAME="$(basename -- "$SITE_ROOT")"
    SUPERVISOR_CONFIG_FILENAME="$(getSuperVisorConfigFileName "$SITE_NAME")"
    TARGET_CRONTAB_FILENAME="$(getTargetCrontabFileName "$SITE_NAME")"

    if [[ -f "$SUPERVISOR_CONFIG_FILENAME" ]]; then
      echo "Crontab \"$SITE_ROOT${CRONTAB_RELATIVE_PATH}\" removed, stopping cron" | logForSite "$SITE_NAME"
      stopCronForSite "$SITE_NAME"
      rm -- "$TARGET_CRONTAB_FILENAME"
    elif [[ -f "$TARGET_CRONTAB_FILENAME" ]]; then
      echo "Crontab \"$SITE_ROOT${CRONTAB_RELATIVE_PATH}\" removed" | logForSite "$SITE_NAME"
      rm -- "$TARGET_CRONTAB_FILENAME"
    fi
  done

supervisorctl update | addTs
