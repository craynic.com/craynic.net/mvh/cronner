#!/usr/bin/env bash

set -Eeuo pipefail

source "${BASH_SOURCE%/*}/common.sh"

LOG_FILENAME="$1"

SITE_NAME="$(basename -- "${LOG_FILENAME%"$LOG_FILE_RELATIVE_PATH"}")"
SUPERVISOR_PROGRAM_NAME="$(getSuperVisorProgramName "$SITE_NAME")"

/usr/bin/supervisorctl signal USR2 "$SUPERVISOR_PROGRAM_NAME"
