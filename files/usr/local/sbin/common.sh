#!/usr/bin/env bash

set -Eeuo pipefail

PROJECTS_ROOT="/var/www/"
CRONTAB_RELATIVE_PATH="/${APP_CRONTAB_RELATIVE_PATH#"/"}"
LOG_FILE_RELATIVE_PATH="/${APP_LOG_FILE_RELATIVE_PATH#"/"}"

getSuperVisorProgramName() {
  SITE_NAME="$1"
  echo -n "supercronic-site-$SITE_NAME"
}

getSuperVisorConfigFileName() {
  SITE_NAME="$1"
  echo -n "/etc/supervisor.d/$(getSuperVisorProgramName "$SITE_NAME").ini"
}

getSiteRoot() {
  SITE_NAME="$1"
  echo -n "${PROJECTS_ROOT}${SITE_NAME}"
}

getSiteID() {
  SITE_NAME="$1"
  stat -c "%g" -- "$(getSiteRoot "$SITE_NAME")"
}

getSiteUserName() {
  SITE_NAME="$1"
  echo -n "site$(getSiteID "$SITE_NAME")"
}

getLogFileName() {
  SITE_NAME="$1"
  echo -n "$(getSiteRoot "$SITE_NAME")${LOG_FILE_RELATIVE_PATH}"
}

getSourceCrontabFileName() {
  SITE_NAME="$1"
  echo -n "$(getSiteRoot "$SITE_NAME")${CRONTAB_RELATIVE_PATH}"
}

getTargetCrontabFileName() {
  SITE_NAME="$1"
  echo -n "/tmp/crontab-$SITE_NAME"
}

ensureUser() {
  SITE_NAME="$1"
  SITE_ROOT="$(getSiteRoot "$SITE_NAME")"
  SITE_ID="$(getSiteID "$SITE_NAME")"
  SITE_USERNAME="$(getSiteUserName "$SITE_NAME")"

  if ! getent passwd "$SITE_ID" >/dev/null; then
    adduser -h "$SITE_ROOT" -s /bin/false -u "$SITE_ID" -D -H -- "$SITE_USERNAME"
  fi
}

createSuperVisorConfig() {
  SITE_NAME="$1"
  SUPERVISOR_CONFIG_FILENAME="$(getSuperVisorConfigFileName "$SITE_NAME")"
  LOG_FILENAME="$(getLogFileName "$SITE_NAME")"

  ensureUser "$SITE_NAME"

  cat > "$SUPERVISOR_CONFIG_FILENAME" <<EOF
[program:$(getSuperVisorProgramName "$SITE_NAME")]
command=bash -c "touch \"$LOG_FILENAME\"; exec /usr/bin/supercronic -- \"$(getTargetCrontabFileName "$SITE_NAME")\" >>\"$LOG_FILENAME\" 2>&1"
stdout_logfile=/dev/null
stdout_logfile_maxbytes=0
stderr_logfile=/dev/null
stderr_logfile_maxbytes=0
user=$(getSiteID "$SITE_NAME")
umask=027
stopasgroup=true
killasgroup=true
autostart=true
EOF
}

validateTargetCrontab() {
  SITE_NAME="$1"
  TARGET_CRONTAB_FILENAME="$(getTargetCrontabFileName "$SITE_NAME")"

  supercronic -debug -quiet -test <(cat -- "$TARGET_CRONTAB_FILENAME") 2>&1 | logForSite "$SITE_NAME"
}

logForSite() {
  SITE_NAME="$1"
  LOG_FILENAME="$(getLogFileName "$SITE_NAME")"

  if [[ ! -f "$LOG_FILENAME" ]]; then
    SITE_ID="$(getSiteID "$SITE_NAME")"

    touch -- "$LOG_FILENAME"
    chown "$SITE_ID:$SITE_ID" -- "$LOG_FILENAME"
  fi

  addTs | tee -a "$LOG_FILENAME"
}

addTs() {
  cat | ts "%F %.T%z"
}

stopCronForSite() {
  SITE_NAME="$1"
  SUPERVISOR_CONFIG_FILENAME="$(getSuperVisorConfigFileName "$SITE_NAME")"

  if [[ -f "$SUPERVISOR_CONFIG_FILENAME" ]]; then
    supervisorctl stop "$(getSuperVisorProgramName "$SITE_NAME")" | addTs
    rm -- "$SUPERVISOR_CONFIG_FILENAME"
  fi
}

assertTargetCrontabNotEmpty() {
  SITE_NAME="$1"
  TARGET_CRONTAB_FILENAME="$(getTargetCrontabFileName "$SITE_NAME")"

  grep -q "[^[:space:]]" "$TARGET_CRONTAB_FILENAME"
}

reloadCronForSite() {
  SITE_NAME="$1"
  supervisorctl signal USR2 "$(getSuperVisorProgramName "$SITE_NAME")" | addTs
}
