#!/usr/bin/env bash

set -Eeuo pipefail

exec /usr/bin/supervisord --nodaemon --configuration /etc/supervisord.conf
