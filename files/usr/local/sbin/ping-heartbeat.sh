#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -n "$APP_HEARTBEAT_ENDPOINT" ]]; then
  curl -s "$APP_HEARTBEAT_ENDPOINT"
  echo "Ping heartbeat" | ts "%F %.T%z"
fi
