#!/usr/bin/env bash

set -Eeuo pipefail

if [[ -n "$APP_HEARTBEAT_ENDPOINT" ]]; then
  printf "\n%s /usr/local/sbin/ping-heartbeat.sh\n" "$APP_HEARTBEAT_CRON" >> /etc/crontabs/root
  echo "Heartbeat enabled"
fi
